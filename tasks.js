'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

function toggleMenu() {
  const menu = document.querySelector('.menu');
  if (menu) {
    menu.classList.toggle('menu-opened');
  }
}

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/

function detectCollision(clickCoord, movedBlockSize, boxSize) {
  const xRightOffset = clickCoord.x + movedBlockSize.width / 2;
  const xLeftOffset = clickCoord.x - movedBlockSize.width / 2;
  const yUpOffset = clickCoord.y + movedBlockSize.height / 2;
  const yDownOffset = clickCoord.y - movedBlockSize.height / 2;

  return (xRightOffset > boxSize.width || xLeftOffset < 0
    || yUpOffset > boxSize.height || yDownOffset < 0);
}

function moveBlock(event) {
  if (event.target.className === 'field') {
    const clickCoord = { x: event.offsetX, y: event.offsetY };
    const boundingClientRect = event.target.getBoundingClientRect();
    const boxSize = { height: boundingClientRect.height, width: boundingClientRect.width };
    let movedBlock = document.querySelector('.moved-block');
    const movedBlockSize = { height: movedBlock.offsetHeight, width: movedBlock.offsetWidth };
    const centerX = clickCoord.x - movedBlockSize.width / 2;
    const centerY = clickCoord.y - movedBlockSize.height / 2;

    if (movedBlock && !detectCollision(clickCoord, movedBlockSize, boxSize)) {
      movedBlock.style.transform = `translate(${centerX}px, ${centerY}px)`;
    }
  }
}

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/

function hideMessage(elem) {
  elem.style.display = 'none';
}

function onClickMessagerHandler(event) {
  if (event.target.tagName === 'SPAN') {
    hideMessage(event.target.parentElement);
  }
}

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/

function onClickLinksHandler(event) {
  if (event.target.tagName === 'A') {
    if (!confirm("Вы уверены что хотите перейти по ссылке?")) {
      event.preventDefault();
    }
  }
}

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
const header = document.querySelector('#taskHeader');

function onChangeFieldHeader(event) {
  header.innerText = event.target.value;
}